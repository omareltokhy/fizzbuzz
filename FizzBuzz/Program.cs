﻿using System;

namespace FizzBuzz
{
    class Project
    {

        static void Main(string[] args)
        {
            for (int i = 1; i < 100; i++)
            {
                Console.Write(Convert(i)+", ");
            }
            Console.WriteLine(Convert(100));
        }
        static string Convert(int number)
        {
            if (number % 3 == 0 && number % 5 == 0)
            {
                return "FizzBuzz";
            }
            else if (number % 3 == 0)
            {
                return "Fizz";
            }
            else if (number % 5 == 0)
            {
                return "Buzz";
            }
            else
            {
                return number.ToString();
            }
        }
    }
}
